export class Palindrome {

    isPalindrome(phrase) {
        var regex = /[\W_]/g;
        var cadena = phrase.toLowerCase().replace(regex, '');
        var phraseReverse = cadena.split('').reverse().join(''); 
        return phraseReverse === cadena ? "is palindrome" : "is not palindrome";
    }
}