export class NumberUtils {

    isEvenOrOdd(number) {
        return number % 2 === 0 ? "Is even" : "Is odd"
    }
}